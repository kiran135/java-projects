import util.Graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class DFS {
    public static Graph graph;
    public static void main(String[] args){
        graph = new Graph(8);
        graph = graph.generateSampleGraph();
        getDFS(graph, 5);
        topologicalSort(graph);
        //minHops();
    }

    public static void topologicalSort(Graph graph){
        int[] visited = new int[graph.n];
        Stack stack = new Stack();
        for (int i = 0; i < graph.n; i++) {
            topologicalSortHelper(graph, i, stack, visited);
        }
        while (!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }

    public static void topologicalSortHelper(Graph graph, int node, Stack stack, int[] visited){
        if(visited[node] == 1){
            return;
        }
        visited[node] = 1;
        for (int i : graph.adjacencyList[node]) {
            topologicalSortHelper(graph,i, stack, visited);
        }
        stack.push(node);
    }

    public static void getDFS(Graph graph, int start){
        int[] visited = new int[graph.n];
        DFSHelper(graph, start, visited);
        for (int i = 0; i < graph.n; i++) {
            DFSHelper(graph, i, visited);
        }

    }

    public static void DFSHelper(Graph graph, int node, int[] visited){
        if(visited[node] == 1){
            return;
        }
        visited[node] = 1;
        System.out.println(node);
        for (int i : graph.adjacencyList[node]) {
            DFSHelper(graph,i,visited);
        }
    }

    public static void minHops(){
        char[][] grid = new char[][] {{ '0', '*', '0', 'd' },
            { '*', '0', '*', '*' },
            { '0', '*', '*', '*' },
            { 's', '*', '*', '*' }};
        boolean[][] visited = new boolean[4][4];
        System.out.println(traverse(grid, 3,0, visited));
    }

    public static int traverse(char[][] grid, int i, int j, boolean[][] visited){
//        if(i>3 || j>3 || i<0 || j<0 || grid[i][j] == '0'){
//            return 0;
//        }
        visited[i][j] = true;
//        if(grid[i][j] == 'd'){
//            return 1;
//        }
        List<Integer> values = new ArrayList<>();
        if(i+1 < 4 && (grid[i+1][j] == '*') && !visited[i+1][j]){
            values.add(1+ traverse(grid, i+1, j, visited));
        }
        if(i-1 >0 && (grid[i-1][j] == '*') && !visited[i-1][j]){
            values.add(1+ traverse(grid, i-1, j, visited));
        }
        if(j+1 < 4 && (grid[i][j+1] == '*') && !visited[i][j+1]){
            values.add(1+ traverse(grid, i, j+1, visited));
        }
        if(j-1 > 0 && (grid[i][j-1] == '*') && !visited[i][j-1]){
            values.add(1+ traverse(grid, i, j-1, visited));
        }
        if(values.size() > 0 ){
            System.out.println(values);
            return Collections.min(values);
        }
        System.out.println(values);
        return 0;
    }
}


