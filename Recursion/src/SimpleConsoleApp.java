import java.io.Console;
import java.util.Scanner;

public class SimpleConsoleApp {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){
            System.out.println(scanner.nextLine());
        }

    }
}
