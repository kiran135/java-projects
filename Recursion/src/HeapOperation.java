import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HeapOperation {
    public static void main(String[] args){
        int arr[] = {12, 11, 13, 5, 6, 7}; //5,6,7,11,12,13,
        //heapSort(arr);
        //findTheKthSmallest(arr, 5);
    }

    private static int[] heapSort(int[] arr) {
        int n = arr.length;
        sort(arr, n);
        for (int anArr : arr) {
            System.out.print(anArr + ",");
        }
        return arr;
    }

    private static void heapify(int[] arr, boolean isMinHeap, int length, int rootIndex){
        int traget = rootIndex;
        int left = 2*rootIndex + 1;
        int right = 2*rootIndex + 2;
        if(isMinHeap){
            if(left < length && arr[left] < arr[traget]){
                traget = left;
            }
            if(right < length && arr[right] < arr[traget]){
                traget = right;
            }
        }
        else{
            if(left < length && arr[left] > arr[traget]){
                traget = left;
            }
            if(right < length && arr[right] > arr[traget]){
                traget = right;
            }
        }

        if(rootIndex != traget){
            int temp = arr[rootIndex];
            arr[rootIndex] = arr[traget];
            arr[traget] = temp;
            heapify(arr, isMinHeap, length, traget);
        }
    }

    private static void sort(int[] arr, int n){
        for(int i = n/2 -1; i >= 0; i--){
            heapify(arr, false, n, i);
        }
        for (int anArr : arr) {
            System.out.print(anArr + ",");

        }
        System.out.println("The max heap is printed.. It took O(nlogn) so far.. since the insertion in the heap takes O(logn)" +
                "and we did it for n elements so its O(nlogn)");
        for(int i = n-1; i >= 0; i--){
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, false, i, 0);

        }
        System.out.println("It took O(nlogn) so far.. since the deleted the root element and rearranged the heap takes O(logn) " +
                "and we did it for n elements so its O(nlogn)");
        System.out.println("The total run time is O(nlogn) + O(nlogn) = O(2nlogn) = O(nlogn)");
    }

    private static int findTheKthSmallest(int[] arr, int k){
        if(k>arr.length){
            return 0;
        }
        for(int i = arr.length/2 -1; i >=0 ; i--){
            heapify(arr, true, arr.length, i);
        }
        for (int anArr : arr) {
            System.out.print(anArr + ",");

        }
        System.out.println();
        for(int i = arr.length-1; i >= arr.length -k; i--){
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, true, i, 0);
        }
        for (int anArr : arr) {
            System.out.print(anArr + ",");

        }
        System.out.println("The Kth smallest is " + arr[arr.length-k]);
        return arr[arr.length-k];
    }
}
