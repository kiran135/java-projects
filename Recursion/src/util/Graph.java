package util;

import java.util.LinkedList;

public class Graph {
    public int n;
    public LinkedList<Integer>[] adjacencyList;
    public Graph(int nodesCount){
        this.n = nodesCount;
        this.adjacencyList = new LinkedList[nodesCount];
        for (int i = 0; i <nodesCount; i++){
            adjacencyList[i] = new LinkedList();
        }
    }
    public void addEdge(int u, int v){
        adjacencyList[u].add(v);
    }

    public Graph generateSampleGraph(){
        this.addEdge(5,2);
        this.addEdge(2,3);
        this.addEdge(3,1);
        this.addEdge(5,0);
        this.addEdge(4,0);
        this.addEdge(4,1);
        this.addEdge(6,7);
        return this;
    }
}

