public class SortOperations {
    public static void main(String args[]){
        int arr[] = {12, 11, 13, 5, 6, 7}; //5,6,7,11,12,13,
        quickSort(arr, 0, arr.length-1);
        for (int anArr : arr) {
            System.out.print(anArr + ",");
        }
    }

    private static int partition(int[] arr, int left, int right){
        int pivot = arr[right];
        int i = left;
        for(int j = left; j <= right-1; j++){
            if(arr[j] <= pivot){
                int temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
                i++;
            }
        }
        int temp = arr[i];
        arr[i] = arr[right];
        arr[right] = temp;
        return i;
    }

    private static void quickSort(int[] arr, int left, int right){
        if(left<right){
            int pivotIndex = partition(arr,left,right);
            quickSort(arr, left, pivotIndex-1);
            quickSort(arr, pivotIndex+1, right);
        }
    }

    private void partition1(int[] arr, int left, int right){
        int pivotIndex = (left+right)/2;
        int pivot = arr[pivotIndex];
        int temp = arr[right];
        arr[right] = arr[pivotIndex];
        arr[pivotIndex] = temp;
        int rightIndex = right-1;
        int leftIndex = left;
        if(leftIndex >= arr.length || rightIndex >= arr.length || leftIndex >= rightIndex){
            return;
        }
        while(leftIndex < rightIndex){
            if(arr[leftIndex] > pivot && arr[rightIndex] < pivot){
                temp = arr[leftIndex];
                arr[leftIndex] = arr[rightIndex];
                arr[rightIndex] = temp;
            }
            leftIndex++;
            rightIndex--;
        }
        temp = arr[pivotIndex];
        arr[pivotIndex] = pivot;
        arr[right] = temp;
    }
}
