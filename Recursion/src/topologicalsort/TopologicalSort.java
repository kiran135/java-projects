package topologicalsort;
import util.Graph;

import java.util.HashMap;
import java.util.Stack;

class Process extends Graph{
    public Process(int nodesCount){
        super(nodesCount);
    }
    public HashMap<Integer, Integer> durations;
    public void setDurations(int key, int value){
        this.durations.put(key,value);
    }
    public void setSampleDurations(){
        this.setDurations(1,5);
        this.setDurations( 2, 7);
        this.setDurations( 3, 8);
        this.setDurations(4,20);
        this.setDurations(5,1);
        this.setDurations(6,1);
    }

    public void establishDependencies(){
        adjacencyList[1].add(2);
        adjacencyList[2].add(3);
        adjacencyList[4].add(5);
    }
}

public class TopologicalSort {
    public static Graph graph;
    public static void main(String[] args){
        graph = new Graph(8);
        graph = graph.generateSampleGraph();
        topologicalSort(graph);
    }

    public static void topologicalSort(Graph graph){
        int[] visited = new int[graph.n];
        Stack stack = new Stack();
        for (int i = 0; i < graph.n; i++) {
            topologicalSortHelper(graph, i, stack, visited);
        }
        while (!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }

    public static void topologicalSortHelper(Graph graph, int node, Stack stack, int[] visited){
        if(visited[node] == 1){
            return;
        }
        visited[node] = 1;
        for (int i : graph.adjacencyList[node]) {
            topologicalSortHelper(graph,i, stack, visited);
        }
        stack.push(node);
    }

    public static void performProcessOrdering(){
        Process process = new Process(8);
        process.establishDependencies();
        process.setSampleDurations();


    }



}
