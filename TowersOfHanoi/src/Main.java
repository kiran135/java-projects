import java.util.Stack;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Stack<Integer> origin = new Stack<>();
        Stack<Integer> buffer = new Stack<>();
        Stack<Integer> destination = new Stack<>();
        for (int i=1 ; i <=2; i++){
            System.out.println(i);
            origin.push(i);
        }
        move(origin.size(), origin, buffer, destination);
        for (Integer i: destination){
            System.out.println(i);
        }
    }

    public static void move(int disks, Stack<Integer> origin, Stack<Integer> buffer, Stack<Integer> destination){
        if(disks <= 0) return;
        move(disks-1, origin, buffer, destination);
        moveTop(origin, destination);
        move(disks-1, buffer, destination, origin);
    }

    public static void moveTop(Stack<Integer> source, Stack<Integer> destination){
        int top = source.pop();
        destination.push(top);
    }

}
